terraform {
  backend gcs {
    bucket = "murygin-kubernetes-platform-demo-develop-tfstate"
    prefix = "terraform/state"
  }
}
