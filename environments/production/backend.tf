terraform {
  backend gcs {
    bucket = "murygin-kubernetes-platform-demo-production-tfstate"
    prefix = "terraform/state"
  }
}
